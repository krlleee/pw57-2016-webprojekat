﻿using PR_57_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR_57_2016.Controllers
{
    public class ProfileController : Controller
    {
        Dictionary<string, Korisnik> dic = new Dictionary<string, Korisnik>();
        // GET: Profile
        public ActionResult Index()
        {
            return View();
        }

        /*public ActionResult GetUser()
        {
            Korisnik user = (Korisnik)Session["US"];
            return View();
        }*/


        [HttpPost]
        public ActionResult EditUserProfile(Korisnik user)
        {
            Korisnik us = (Korisnik)Session["US"];
            dic = ReadWrite.ReadUser("~/App_Data/Users.txt");
            if(dic.ContainsKey(us.Id))
            {
                user.Id = us.Id;
                user.Tickets = dic[user.Id].Tickets;
                user.Type = dic[user.Id].Type;
                user.Points = dic[user.Id].Points;
                user.Events = dic[user.Id].Events;
                user.Role = dic[user.Id].Role;
                user.LoggedIn = us.LoggedIn;
                user.Deleted = us.Deleted;
                
                user.BirthDate = us.BirthDate;
                dic[us.Id] = user;
            }
            ReadWrite.WriteEdit(dic, "~/App_Data/Users.txt");
            
            Session["US"] = user;
            return RedirectToAction("Index", "Event");

        }
    }
}