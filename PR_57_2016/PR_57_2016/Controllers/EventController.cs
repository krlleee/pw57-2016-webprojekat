﻿using PR_57_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PR_57_2016.Controllers
{
    public class EventController : Controller
    {
        
        Dictionary<string, Event> events = new Dictionary<string, Event>();
        Dictionary<string, Event> eventsSort = new Dictionary<string, Event>();
        public static Dictionary<string, Event> sort = new Dictionary<string, Event>();
        public static Dictionary<string, Ticket> ticSort = new Dictionary<string, Ticket>();

        // GET: Event
        public ActionResult Index()
        {   
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");

            DictionarySort(events);
            ViewBag.Events = eventsSort;
            return View();
        }

        public ActionResult AddEvent()
        {
            Dictionary<string, EventPlace> places = new Dictionary<string, EventPlace>();
            places = ReadWrite.ReadPlace("~/App_Data/EventLocations.txt");
            ViewBag.Places = places;

            string today = DateTime.Today.ToString("yyyy-MM-ddThh:mm");
            ViewBag.Today = today;

            if (TempData["status"] != null)
            {
                ViewBag.Status = "Location is alredy reserved for another event!";
                TempData.Remove("status");
            }

            if(TempData["remaining"]!=null)
            {
                ViewBag.Remaining = TempData["remaining"].ToString();
                TempData.Remove("remaining");
            }
          

            return View();
        }

        [HttpPost]
        public ActionResult AddEvent(EventHelp eventt)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Dictionary<string, EventPlace> places = new Dictionary<string, EventPlace>();

            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");
            places = ReadWrite.ReadPlace("~/App_Data/EventLocations.txt");

            EventPlace evpl = new EventPlace();
            

            Event newEvent = new Event();

            if(eventt.TicketRem>eventt.SeatsNum)
            {
                TempData["remaining"] = "Number of seats must be greater than remaining number of tickets!";
                return RedirectToAction("AddEvent", "Event");
            }

            foreach(var place in places)

            {
                if(eventt.Place==place.Value.Place)
                {
                    evpl.Id= place.Key;
                    evpl.Place = place.Value.Place;
                   
                }
               
            }


            newEvent.Address = evpl;
            newEvent.Name = eventt.Name;
            newEvent.Poster = eventt.Poster;
            newEvent.Price = eventt.Price;
            newEvent.SeatsNum = eventt.SeatsNum;
            newEvent.SellerId = eventt.SellerId;
            newEvent.Id = eventt.Id;
            newEvent.Status = eventt.Status;
            newEvent.TicketRem = eventt.TicketRem;
            newEvent.Type = eventt.Type;
            newEvent.Date = eventt.Date;

            foreach (var ev in events)
            {
                if (ev.Value.Date.ToShortDateString() == newEvent.Date.ToShortDateString() && ev.Value.Address.Id == newEvent.Address.Id)
                {




                    TempData["status"] = "Location is alredy reserved for another event!";
                    
                    
                    return RedirectToAction("AddEvent");
                }
            }
            

            Korisnik us = (Korisnik)Session["US"];
            ReadWrite.WriteEvent(newEvent, "~/App_Data/Events.txt",us.Id);
            
            return RedirectToAction("Index", "Event");
        }


        private void DictionarySort(Dictionary<string, Event> dict)
        {
            var dictSort = from objDict in dict orderby objDict.Value.Date  descending select objDict;
            
                eventsSort = dictSort.ToDictionary(t=>t.Key,t=>t.Value);
                
        }

        public ActionResult SellerEvents()
        {
            Korisnik user = (Korisnik)Session["US"];
            Dictionary<string,Event> events = ReadWrite.ReadEvent("~/App_Data/Events.txt");
            Dictionary<string, Event> sellerEv = new Dictionary<string, Event>();

            foreach(var ev in events)
            {
                if (ev.Value.SellerId == user.Id)
                {
                    sellerEv[ev.Key] = events[ev.Key];
                }
            }

            DictionarySort(sellerEv);
            ViewBag.Seller = eventsSort;

            return View();
        }

        public ActionResult EditEvent(string id)
        {
            Dictionary<string, EventPlace> places = new Dictionary<string, EventPlace>();
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");
            places = ReadWrite.ReadPlace("~/App_Data/EventLocations.txt");
            Event ev = new Event();



            foreach (var eve in events)
            {
                if (eve.Value.Id == id)
                {
                    ev = events[id];
                }
            }

            if (TempData["remaining"] != null)
            {
                ViewBag.Remaining = TempData["remaining"].ToString();
                TempData.Remove("remaining");
            }
            if (TempData["status"] != null)
            {
                ViewBag.Status = "Location is alredy reserved for another event!";
                TempData.Remove("status");
            }

            string date = ev.Date.ToString("yyyy-MM-ddThh:mm");

            string today = DateTime.Today.ToString("yyyy-MM-ddThh:mm");

            ViewBag.Today = today;
            ViewBag.Date = date;
            ViewBag.Event = ev;
            ViewBag.Places=places;
            return View();
        }

        [HttpPost]
        public ActionResult EditEvent(EventHelp eventt)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Dictionary<string, EventPlace> places = new Dictionary<string, EventPlace>();

            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");
            places = ReadWrite.ReadPlace("~/App_Data/EventLocations.txt");

           string date= eventt.Date.ToString();

            EventPlace evpl = new EventPlace();


            Event newEvent = new Event();

            if (eventt.TicketRem > eventt.SeatsNum)
            {
                TempData["remaining"] = "Number of seats must be greater than remaining number of tickets!";
                return RedirectToAction("EditEvent", new { id = eventt.Id });
            }

            foreach (var place in places)

            {
                if (eventt.Place == place.Value.Place)
                {
                    evpl.Id = place.Key;
                    evpl.Place = place.Value.Place;

                }

            }

            newEvent.Address = evpl;
            newEvent.Name = eventt.Name;
            newEvent.Poster = eventt.Poster;
            newEvent.Price = eventt.Price;
            newEvent.SeatsNum = eventt.SeatsNum;
            newEvent.SellerId = eventt.SellerId;
            newEvent.Id = eventt.Id;
            newEvent.Status = eventt.Status;
            newEvent.TicketRem = eventt.TicketRem;
            newEvent.Type = eventt.Type;
            newEvent.Date = eventt.Date;

            foreach (var ev in events)
            {
                if (ev.Value.Date.ToShortDateString() == newEvent.Date.ToShortDateString() && ev.Value.Address.Id == newEvent.Address.Id)
                {




                    TempData["status"] = "Location is alredy reserved for another event!";


                    return RedirectToAction("EditEvent", new { id = eventt.Id });
                }
            }

            events[eventt.Id] = newEvent;

            ReadWrite.WriteEditEvent(events, "~/App_Data/Events.txt");

            return RedirectToAction("SellerEvents", "Event");
        }

        public ActionResult EventInfo(string id)
        {
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");
            List<Comment> comm = new List<Comment>();
            List<Comment> co= new List<Comment>();


           
            if(TempData["empty"]!=null)
            {
                ViewBag.Error = "Please insert comment!";
            }
            if(TempData["stop"]!=null)
            {
                ViewBag.Stop = "You cannot insert comment!";
                TempData.Remove("stop");
            }
            if(TempData["price"]!=null)
            {
                int price = Int32.Parse(TempData["price"].ToString());
                ViewBag.Price = price;
                TempData.Remove("price");
            }
            if(TempData["numeror"]!=null)
            {
                ViewBag.Num = TempData["numeror"].ToString();
                TempData.Remove("numeror");
            }
            if (TempData["ticket"] != null)
            {
                ViewBag.Ticket = TempData["ticket"];
                TempData.Remove("ticket");
            }

            comm = ReadWrite.ReadComment("~/App_Data/Comments.txt");
            Event ev = new Event();

            foreach(var eve in events)
            {
                if(eve.Value.Id==id)
                {
                    ev = events[id];
                }
            }

            ViewBag.Event = ev;

            foreach(var com in comm)
            {
                if(com.EventId==ev.Id)
                {
                    co.Add(com);
                }
            }

            double grade;
            int sum=0;
            int count = 0;

            foreach(var coom in co)
            {
                if(coom.Aproved==CommentEnum.APROVED)
                {
                    sum = sum + coom.Grade;
                    count++;
                }
                
            }

            double suma = Convert.ToDouble(sum);
            double countt = Convert.ToDouble(count);

            grade = suma / countt;
            grade = Math.Round(grade, 2);

            ViewBag.Comments = co;
            ViewBag.Grade = grade;
            return View();
        }

        public ActionResult SearchEvent()
        {

            Dictionary<string, EventPlace> places = new Dictionary<string, EventPlace>();
            places = ReadWrite.ReadPlace("~/App_Data/EventLocations.txt");
            ViewBag.Places = places;

            if(TempData["send"]!=null)
            {
                ViewBag.Find = TempData["send"];
            }

            if(TempData["Error"]!=null)
            {
                ViewBag.Error = "No events found for search parameters";
                TempData.Remove("Error");
            }

            if(TempData["Errorp"] != null)
            {
                ViewBag.Errorp = "You must enter at least one search parameter";
                TempData.Remove("Errorp");
            }
            if(TempData["FilterErorr"]!=null)
            {
                ViewBag.Filter = "No events found for filter";
                TempData.Remove("FilterErorr");
            }

            return View();
        }

        [HttpPost]
        public ActionResult SearchEvent(SearchHelp sHelp)
        {
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");
            Dictionary<string, Event> find = new Dictionary<string, Event>();

            if (sHelp.Date1.ToString() == "1/1/0001 00:00:00" && sHelp.Date2.ToString() == "1/1/0001 00:00:00" && sHelp.Name == null && sHelp.Place == "Choose place" && sHelp.price1 == 0 && sHelp.price2 == 0)
            {
                TempData["Errorp"] = "You must enter at least one search parameter";
                RedirectToAction("SearchEvent", "Event");
            }
            else
            {
                foreach (var ev in events)
                {
                    if ((ev.Value.Address.Place == sHelp.Place || sHelp.Place == "Choose place") && (ev.Value.Name == sHelp.Name || sHelp.Name == null) && (ev.Value.Date >= sHelp.Date1 || sHelp.Date1.ToString() == "1/1/0001 00:00:00") && (ev.Value.Date <= sHelp.Date2 || sHelp.Date2.ToString() == "1/1/0001 00:00:00") && (ev.Value.Price >= sHelp.price1 || sHelp.price1 == 0) && (ev.Value.Price <= sHelp.price2 || sHelp.price2 == 0))
                    {
                        find[ev.Key] = events[ev.Key];
                    }
                }

                
                if (find.Count == 0)
                {
                    TempData["Error"] = "No events found for search parameters";
                }
                else
                {
                    TempData["send"] = find;
                    sort = find;
                }
            }


            return RedirectToAction("SearchEvent","Event");
        }

        [HttpPost]
        public ActionResult SortEvent(SortHelp type)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            events = sort;

            string tip = type.SortType;

            switch(tip){
                case "nameaz":

                    var dictSort = from objDict in events orderby objDict.Value.Name ascending select objDict;

                    eventsSort = dictSort.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = eventsSort;
                    break;

                case "nameza":

                    var dictSort1 = from objDict in events orderby objDict.Value.Name descending select objDict;

                    eventsSort = dictSort1.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = eventsSort;
                    break;

                case "dateasc":

                    var dictSort2 = from objDict in events orderby objDict.Value.Date ascending select objDict;

                    eventsSort = dictSort2.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = eventsSort;
                    break;

                case "datedsc":

                    var dictSort3 = from objDict in events orderby objDict.Value.Date descending select objDict;

                    eventsSort = dictSort3.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = eventsSort;
                    break;

                case "priceasc":

                    var dictSort4 = from objDict in events orderby objDict.Value.Price ascending select objDict;

                    eventsSort = dictSort4.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = eventsSort;
                    break;

                case "pricedsc":

                    var dictSort5 = from objDict in events orderby objDict.Value.Price descending select objDict;

                    eventsSort = dictSort5.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = eventsSort;
                    break;

                case "placeasc":

                    var dictSort6 = from objDict in events orderby objDict.Value.Address.Place ascending select objDict;

                    eventsSort = dictSort6.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = eventsSort;
                    break;

                case "placedsc":

                    var dictSort7 = from objDict in events orderby objDict.Value.Address.Place descending select objDict;

                    eventsSort = dictSort7.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = eventsSort;
                    break;
            }

            return RedirectToAction("SearchEvent", "Event");
        }

        [HttpPost]
        public ActionResult AddComment(Comment com)
        {
            List<Comment> comm = new List<Comment>();
            Dictionary<string, Ticket> tickets = new Dictionary<string, Ticket>();

            tickets = ReadWrite.ReadTicket("~/App_Data/Tickets.txt");

            Korisnik user = (Korisnik)Session["US"];
            com.UserId = user.Id;
            com.Username = user.Username;

            if (com.Text == null)
            {
                TempData["empty"] = "Please insert comment!";
                return RedirectToAction("EventInfo", new { id = com.EventId });
            }
            else
            {
                foreach (var tic in tickets)
                {
                    if (tic.Value.Eventt == com.EventId && tic.Value.BuyerName == com.UserId && tic.Value.Status==TicketStatus.RESERVED)
                    {

                        ReadWrite.WriteComment(com, "~/App_Data/Comments.txt");
                        return RedirectToAction("EventInfo", new { id = com.EventId });
                    }

                }
               
                    TempData["stop"] = "You cannot insert comment!";
                    return RedirectToAction("EventInfo", new { id = com.EventId });
               
            }

           
        }

        public ActionResult FilterEvent(FilterEvent filter)
        {
            string type = filter.FilterType;
            Dictionary<string, Event> prefiltered = new Dictionary<string, Event>();
            Dictionary<string, Event> filtered = new Dictionary<string, Event>();
            prefiltered = sort;

            if (type == "concert")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Type == EventType.CONCERT)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else if (type == "theatre")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Type == EventType.THEATRE)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else if (type == "cinema")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Type == EventType.CINEMA)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else if (type == "festival")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Type == EventType.FESTIVAL)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.TicketRem > 0)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }

            if (filtered.Count == 0)
            {
                TempData["FilterErorr"] = "Nothing found!";
            }
            else
            {
                sort = filtered;
                TempData["send"] = filtered;
            }
           

            return RedirectToAction("SearchEvent", "Event");
        }


        [HttpPost]
        public ActionResult BuyTicket(Ticket ticket)
        {
            int price = 0;

            Event ev = new Event();
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();

            users = ReadWrite.ReadUser("~/App_Data/Users.txt");
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");

            foreach(var eve in events)
            {
                if(eve.Value.Id==ticket.Eventt)
                {
                    if(eve.Value.TicketRem-ticket.Amount<0)
                    {

                        TempData["numeror"] = "There is not enough tickets";
                        return RedirectToAction("EventInfo", new { id = ticket.Eventt });
                    }
                }
            }


            if (ticket.Type == TicketType.REGULAR)
            {
                price = ticket.Amount * ticket.Price;
            }
            else if (ticket.Type == TicketType.FANPIT)
            {
                price = ticket.Amount * ticket.Price * 2;
            }
            else
            {
                price = ticket.Amount * ticket.Price * 4 ;
            }

            foreach(var user in users)
            {
                if(user.Value.Id==ticket.BuyerName)
                {
                    if (user.Value.Type == UserType.BEGGINER)
                    {

                    }
                    else if (user.Value.Type == UserType.BRONZE)
                    {
                        price = price - 2 * price / 100;
                    }
                    else if (user.Value.Type == UserType.SILVER)
                    {
                        price = price - 3 * price / 100;
                    }
                    else
                    {
                        price = price - 4 * price / 100;
                    }
                }
                
            }


            TempData["price"] = price;
            TempData["ticket"] = ticket;
            return RedirectToAction("EventInfo", new { id = ticket.Eventt });
        }

        public ActionResult SubmitRes(Ticket ticket)
        {
            Korisnik user = (Korisnik)Session["US"];
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Dictionary<string, Korisnik> dic = new Dictionary<string, Korisnik>();

            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");

            foreach(var ev in events)
            {
                if(ev.Value.Id==ticket.Eventt)
                {
                    ev.Value.TicketRem = ev.Value.TicketRem - ticket.Amount;

                }
            }

            float points = (float)user.Points;

            points += (float)ticket.Price / 1000*133;

            user.Points = (int)points;

            if(user.Points>=1000 && user.Points<2500)
            {
                user.Type = UserType.BRONZE;
            }
            else if(user.Points>=2500 && user.Points<5000)
            {
                user.Type = UserType.SILVER;
            }
            else if(user.Points>=5000)
            {
                user.Type = UserType.GOLD;
            }
            else
            {
                user.Type = UserType.BEGGINER;
            }


            dic = ReadWrite.ReadUser("~/App_Data/Users.txt");
            if (dic.ContainsKey(user.Id))
            {
                user.Id = user.Id;
                user.Tickets = dic[user.Id].Tickets;
                user.Type = user.Type;
                user.Points = user.Points;
                user.Events = dic[user.Id].Events;
                user.Role = dic[user.Id].Role;
                user.LoggedIn = user.LoggedIn;
                user.Deleted = user.Deleted;

                user.BirthDate = user.BirthDate;
                dic[user.Id] = user;
            }



            ReadWrite.WriteEdit(dic, "~/App_Data/Users.txt");

            ReadWrite.WriteEditEvent(events, "~/App_Data/Events.txt");

            ReadWrite.WriteTicket(ticket, "~/App_Data/Tickets.txt");
            return RedirectToAction("Index", "Event");
        }

        public ActionResult MyTickets()
        {
            Dictionary<string, Ticket> tickets = new Dictionary<string, Ticket>();
            Dictionary<string, TicketHelp> mytickets = new Dictionary<string, TicketHelp>();
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            

            Korisnik user = (Korisnik)Session["US"];
            

            tickets = ReadWrite.ReadTicket("~/App_Data/Tickets.txt");
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");

            foreach(var tic in tickets)
            {
                    if (events.ContainsKey(tic.Value.Eventt)&& tic.Value.BuyerName==user.Id)
                    {
                    TicketHelp ticket = new TicketHelp();
                        ticket.Date = events[tic.Value.Eventt].Date;
                        ticket.NameEvent = events[tic.Value.Eventt].Name;
                        ticket.Price = tic.Value.Price;
                        ticket.Amount = tic.Value.Amount;
                        ticket.Status = tic.Value.Status;
                        ticket.Type = tic.Value.Type;
                        ticket.Id = tic.Value.Id;
                        mytickets.Add(tic.Value.Id, ticket);
                    }
            }

            ViewBag.Tickets = mytickets;
            return View();
        }

        public ActionResult AllTickets()
        {
            Dictionary<string, Ticket> tickets = new Dictionary<string, Ticket>();
            Dictionary<string, TicketHelp> mytickets = new Dictionary<string, TicketHelp>();
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();


            Korisnik user = (Korisnik)Session["US"];


            tickets = ReadWrite.ReadTicket("~/App_Data/Tickets.txt");
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");

            foreach (var tic in tickets)
            {
                if (events.ContainsKey(tic.Value.Eventt))
                {
                    TicketHelp ticket = new TicketHelp();
                    ticket.Date = events[tic.Value.Eventt].Date;
                    ticket.NameEvent = events[tic.Value.Eventt].Name;
                    ticket.Price = tic.Value.Price;
                    ticket.Amount = tic.Value.Amount;
                    ticket.Status = tic.Value.Status;
                    ticket.Type = tic.Value.Type;
                    ticket.Id = tic.Value.Id;
                    foreach(var us in users)
                    {
                        if(users.ContainsKey(tic.Value.BuyerName))
                        {
                            ticket.BuyerName = users[tic.Value.BuyerName].Ime;
                            ticket.BuyerLast = users[tic.Value.BuyerName].Prezime;
                        }
                    }
                    
                    mytickets.Add(tic.Value.Id, ticket);
                }
            }

            ViewBag.Tickets = mytickets;
            return View();
        }

        public ActionResult CancelTicket(string id)
        {
            Korisnik user = (Korisnik)Session["US"];
            Dictionary<string, Ticket> tickets = new Dictionary<string, Ticket>();
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Dictionary<string, Korisnik> dic = new Dictionary<string, Korisnik>();

            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");

            tickets = ReadWrite.ReadTicket("~/App_Data/Tickets.txt");

            foreach(var tic in tickets)
            {
                if (tic.Value.Id == id)
                {
                    tickets[id].Status = TicketStatus.CANCELED;
                    user.Canceled += 1;
                    if (events.ContainsKey(tic.Value.Eventt))
                    {
                        events[tic.Value.Eventt].TicketRem += tic.Value.Amount;
                    }

                    if (tic.Value.BuyerName == user.Id)
                    {
                        float points = (float)user.Points;

                        points -= (float)tic.Value.Price / 1000 * 133 *4;

                        user.Points = (int)points;

                        if (user.Points >= 1000 && user.Points < 2500)
                        {
                            user.Type = UserType.BRONZE;
                        }
                        else if (user.Points >= 2500 && user.Points < 5000)
                        {
                            user.Type = UserType.SILVER;
                        }
                        else if (user.Points >= 5000)
                        {
                            user.Type = UserType.GOLD;
                        }
                        else
                        {
                            user.Type = UserType.BEGGINER;
                        }

                        dic = ReadWrite.ReadUser("~/App_Data/Users.txt");
                        if (dic.ContainsKey(user.Id))
                        {
                            user.Id = user.Id;
                            user.Tickets = dic[user.Id].Tickets;
                            user.Type = user.Type;
                            user.Points = user.Points;
                            user.Events = dic[user.Id].Events;
                            user.Role = dic[user.Id].Role;
                            user.LoggedIn = user.LoggedIn;
                            user.Deleted = user.Deleted;

                            user.BirthDate = user.BirthDate;
                            dic[user.Id] = user;
                        }

                        ReadWrite.WriteEdit(dic, "~/App_Data/Users.txt");
                    }
                }
               
            }


            ReadWrite.WriteEditEvent(events, "~/App_Data/Events.txt");
            ReadWrite.WriteEditTicket(tickets, "~/App_Data/Tickets.txt");

           return RedirectToAction("MyTickets", "Event");
        }

        public ActionResult ActivateComment()
        {
            List<Comment> comm = new List<Comment>();
            comm = ReadWrite.ReadComment("~/App_Data/Comments.txt");
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");
            foreach(var ev in events)
            {
                foreach(var com in comm)
                {
                    if (ev.Value.Id == com.EventId)
                    {
                        com.EventId = ev.Value.Name;
                    }
                }
            }

            ViewBag.Comments = comm;
            return View();
        }

        public ActionResult ActivateComm(string id)
        {
            List<Comment> comments = new List<Comment>();
            comments = ReadWrite.ReadComment("~/App_Data/Comments.txt");

            foreach(var com in comments)
            {
                if (com.Text == id)
                {
                    com.Aproved = CommentEnum.APROVED;
                }
            }

            ReadWrite.CommentApproved(comments, "~/App_Data/Comments.txt");
            return RedirectToAction("ActivateComment", "Event");
        }

        public ActionResult NoComm(string id)
        {
            List<Comment> comments = new List<Comment>();
            comments = ReadWrite.ReadComment("~/App_Data/Comments.txt");

            foreach (var com in comments)
            {
                if (com.Text == id)
                {
                    com.Deleted = true;
                    ReadWrite.CommentApproved(comments, "~/App_Data/Comments.txt");
                    return RedirectToAction("ActivateComment", "Event");
                }
            }
            return RedirectToAction("ActivateComment", "Event");

        }

        public ActionResult SearchTickets()
        {
            if(TempData["Errorp"]!=null)
            {
                ViewBag.Errorp = "You must enter at least one search parameter";
                TempData.Remove("Errorp");
            }
            if(TempData["Error"]!=null)
            {
                ViewBag.Error = "No tickets found for search parameters";
                TempData.Remove("Error");
            }
            if(TempData["send"]!=null)
            {
                ViewBag.Tickets = TempData["send"];
            }
            if(TempData["FilterErorr"]!=null)
            {
                ViewBag.Filter = "No tickets found for filter";
                TempData.Remove("FilterErorr");
            }
            return View();
        }

        [HttpPost]
        public ActionResult SearchTickets(SearchTicket ticket)
        {
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");
            Dictionary<string, Ticket> tickets = new Dictionary<string, Ticket>();
            tickets = ReadWrite.ReadTicket("~/App_Data/Tickets.txt");
            Dictionary<string, Ticket> find = new Dictionary<string, Ticket>();

            Korisnik us = (Korisnik)Session["US"];
            string userId = ticket.UserId;

            if (ticket.DateFrom.ToString() == "1/1/0001 00:00:00" && ticket.DateTo.ToString() == "1/1/0001 00:00:00" && ticket.EventName == null && ticket.PriceFrom == 0 && ticket.PriceTo == 0)
            {
                TempData["Errorp"] = "You must enter at least one search parameter";
                RedirectToAction("SearchTickets", "Event");
            }
            else
            {
                foreach (var ev in events)
                {
                    foreach (var tic in tickets)
                    {
                        if (ev.Value.Id == tic.Value.Eventt)
                        {
                            tic.Value.Eventt = ev.Value.Name;
                        }
                    }
                }
                ViewBag.TIc = tickets;
                foreach (var tic in tickets)
                {
                    if ((tic.Value.Eventt == ticket.EventName || ticket.EventName == null) && (tic.Value.Date >= ticket.DateFrom || ticket.DateFrom.ToString() == "1/1/0001 00:00:00") && (tic.Value.Date <= ticket.DateTo || ticket.DateTo.ToString() == "1/1/0001 00:00:00") && (tic.Value.Price >= ticket.PriceFrom || ticket.PriceFrom == 0) && (tic.Value.Price <= ticket.PriceTo || ticket.PriceTo == 0) && tic.Value.BuyerName==userId)
                    {
                        find[tic.Key] = tickets[tic.Key];
                    }
                }
                
                
                if (find.Count == 0)
                {
                    TempData["Error"] = "No events found for search parameters";
                }
                else
                {
                    TempData["send"] = find;
                    ticSort = find;
                    
                }
            }

            return RedirectToAction("SearchTickets", "Event");
        }

        [HttpPost]

        public ActionResult SortTickets(TicketSortHelp type)
        {
            Dictionary<string, Ticket> tickets = new Dictionary<string, Ticket>();
            Dictionary<string, Ticket> ticketsSort = new Dictionary<string, Ticket>();
            tickets = ticSort;

            string tip = type.SortType;

            switch (tip)
            {
                case "nameaz":

                    var dictSort = from objDict in tickets orderby objDict.Value.Eventt ascending select objDict;

                    ticSort = dictSort.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = ticSort;
                    break;

                case "nameza":

                    var dictSort1 = from objDict in tickets orderby objDict.Value.Eventt descending select objDict;

                    ticSort = dictSort1.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = ticSort;
                    break;

                case "dateasc":

                    var dictSort2 = from objDict in tickets orderby objDict.Value.Date ascending select objDict;

                    ticSort = dictSort2.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = ticSort;
                    break;

                case "datedsc":

                    var dictSort3 = from objDict in tickets orderby objDict.Value.Date descending select objDict;

                    ticSort = dictSort3.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = ticSort;
                    break;

                case "priceasc":

                    var dictSort4 = from objDict in tickets orderby objDict.Value.Price ascending select objDict;

                    ticSort = dictSort4.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = ticSort;
                    break;

                case "pricedsc":

                    var dictSort5 = from objDict in tickets orderby objDict.Value.Price descending select objDict;

                    ticSort = dictSort5.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = ticSort;
                    break;

                
            }

            return RedirectToAction("SearchTickets", "Event");
        }

        [HttpPost]
        public ActionResult FilterTickets(FilterTicket filter)
        {
            string type = filter.FIlterType;
            Dictionary<string, Ticket> prefiltered = new Dictionary<string, Ticket>();
            Dictionary<string, Ticket> filtered = new Dictionary<string, Ticket>();
            prefiltered = ticSort;

            if (type == "vip")
            {
                foreach (var tic in prefiltered)
                {
                    if (tic.Value.Type == TicketType.VIP)
                    {
                        filtered[tic.Key] = prefiltered[tic.Key];
                    }
                }
            }
            else if (type == "fanpit")
            {
                foreach (var tic in prefiltered)
                {
                    if (tic.Value.Type == TicketType.FANPIT)
                    {
                        filtered[tic.Key] = prefiltered[tic.Key];
                    }
                }
            }
            else if (type == "regular")
            {
                foreach (var tic in prefiltered)
                {
                    if (tic.Value.Type == TicketType.REGULAR)
                    {
                        filtered[tic.Key] = prefiltered[tic.Key];
                    }
                }
            }
            else if (type == "canceled")
            {
                foreach (var tic in prefiltered)
                {
                    if (tic.Value.Status == TicketStatus.CANCELED)
                    {
                        filtered[tic.Key] = prefiltered[tic.Key];
                    }
                }
            }
            else 
            {
                foreach (var tic in prefiltered)
                {
                    if (tic.Value.Status == TicketStatus.RESERVED)
                    {
                        filtered[tic.Key] = prefiltered[tic.Key];
                    }
                }
            }
          

            if (filtered.Count == 0)
            {
                TempData["FilterErorr"] = "Nothing found!";
            }
            else
            {
                ticSort = filtered;
                TempData["send"] = filtered;
            }

            return RedirectToAction("SearchTickets", "Event");
        }

    }
}