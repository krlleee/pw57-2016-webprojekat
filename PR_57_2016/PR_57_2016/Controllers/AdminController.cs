﻿using PR_57_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR_57_2016.Controllers
{
    public class AdminController : Controller
    {
        Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
        Dictionary<string, Korisnik> usersSort = new Dictionary<string, Korisnik>();
        Dictionary<string, Event> events = new Dictionary<string, Event>();
        public static Dictionary<string,Korisnik> sort= new Dictionary<string,Korisnik>();
        // GET: Admin
        public ActionResult Index()
        {
            

            if(TempData["send"]!=null)
            {
                ViewBag.Find = TempData["send"];
            }

            if (TempData["Error"] != null)
            {
                ViewBag.Error = "You must enter keyword";
                TempData.Remove("Error");
            }

            if (TempData["Errorp"] != null)
            {
                ViewBag.Errorp = "No users found for keyword";
                TempData.Remove("Errorp");
            }
            if (TempData["FilterErorr"] != null)
            {
                ViewBag.Filter = "Nothing found!";
                TempData.Remove("FilterErorr");
            }

            return View();
        }

        public ActionResult AllUsers()
        {
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");
            
            TempData["send"] = users;
            sort = users;
            return RedirectToAction("Index", "Admin");
        }

        [HttpPost]
        public ActionResult SearchUsers(SearchUserHelp search)
        {

            string str = search.Srch;
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");
            Dictionary<string, Korisnik> find = new Dictionary<string, Korisnik>();


            if (str==null)
            {
                TempData["Error"] = "You must enter keyword";
                RedirectToAction("Index", "Admin");
            }
            else
            {
                foreach (var us in users)
                {
                    if (us.Value.Ime.ToLower().Contains(str) || us.Value.Prezime.ToLower().Contains(str) || us.Value.Username.ToLower().Contains(str))
                    {
                        find[us.Key] = users[us.Key];
                    }
                }

                if (find.Count == 0)
                {
                    TempData["Errorp"] = "No users found for keyword";
                }
                else
                {
                    TempData["send"] = find;
                    sort = find;
                }
            }

            return RedirectToAction("Index", "Admin");
        }

        [HttpPost]
        public ActionResult SortUsers(UserSortHelp sortType)
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();

            string tip = sortType.Shelp;

            switch (tip)
            {
                case "nameaz":

                    var dictSort = from objDict in sort orderby objDict.Value.Ime ascending select objDict;

                    usersSort = dictSort.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "nameza":

                    var dictSort1 = from objDict in sort orderby objDict.Value.Ime descending select objDict;

                    usersSort = dictSort1.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "lastasc":

                    var dictSort2 = from objDict in sort orderby objDict.Value.Prezime ascending select objDict;

                    usersSort = dictSort2.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "lastdsc":

                    var dictSort3 = from objDict in sort orderby objDict.Value.Prezime descending select objDict;

                    usersSort = dictSort3.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "userasc":

                    var dictSort4 = from objDict in sort orderby objDict.Value.Username ascending select objDict;

                    usersSort = dictSort4.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "userdsc":

                    var dictSort5 = from objDict in sort orderby objDict.Value.Username descending select objDict;

                    usersSort = dictSort5.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "pointsasc":

                    var dictSort6 = from objDict in sort orderby objDict.Value.Points ascending select objDict;

                    usersSort = dictSort6.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;

                case "pointsdsc":

                    var dictSort7 = from objDict in sort orderby objDict.Value.Points descending select objDict;

                    usersSort = dictSort7.ToDictionary(t => t.Key, t => t.Value);

                    TempData["send"] = usersSort;
                    break;
            }

            return RedirectToAction("Index", "Admin");
        }

        public ActionResult FilterUsers(UserFilter filter)
        {
            string type = filter.UserFil;
            Dictionary<string, Korisnik> prefiltered = new Dictionary<string, Korisnik>();
            Dictionary<string, Korisnik> filtered = new Dictionary<string, Korisnik>();

            if (sort.Count == 0)
            {
                prefiltered = ReadWrite.ReadUser("~/App_Data/Users.txt");
            }
            else
            {
                prefiltered = sort;
            }


            if (type == "seller")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Role == UserRole.SELLER)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else if (type == "administrator")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Role == UserRole.ADMINISTRATOR)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else if (type == "guest")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Role == UserRole.GUEST)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else if (type == "registrated")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Role == UserRole.REGISTRATED)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else if (type == "beginner")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Type == UserType.BEGGINER)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else if (type == "gold")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Type == UserType.GOLD)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else if (type == "silver")
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Type == UserType.SILVER)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }
            else
            {
                foreach (var ev in prefiltered)
                {
                    if (ev.Value.Type == UserType.BRONZE)
                    {
                        filtered[ev.Key] = prefiltered[ev.Key];
                    }
                }
            }

            if (filtered.Count == 0)
            {
                TempData["FilterErorr"] = "Nothing found!";
            }
            else
            {
                sort = filtered;
                TempData["send"] = filtered;
            }

            
            return RedirectToAction("Index", "Admin");
        }

        public ActionResult ActivateEvent()
        {
           events= ReadWrite.ReadEvent("~/App_Data/Events.txt");

            ViewBag.Events = events;

            return View();
        }

        public ActionResult ActivateEv(string id)
        {
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");

            if(events.ContainsKey(id))
            {
                events[id].Status = EventStatus.ACTIVE;
            }

            ReadWrite.EventActive(events, "~/App_Data/Events.txt");

            return RedirectToAction("ActivateEvent","Admin");
        }

        public ActionResult AddSeller()
        {
            string today = DateTime.Today.ToString("yyyy-MM-dd");
            ViewBag.Today = today;
            return View();
        }


        [HttpPost]
        public ActionResult AddSeller(Korisnik user)
        {
            Dictionary<string, Korisnik> us = (Dictionary<string, Korisnik>)HttpContext.Application["US"];


            foreach (var use in us)
            {
                if(use.Value.Username==user.Username)
                {
                    ViewBag.Mess = "User with that username already exists!";
                    return View("AddSeller");
                }
            }
            if (user.Username != "" && user.Username.Count() < 4)
            {
                ViewBag.Mess = "Username must have at least 4 symbols!";
                return View("AddSeller");
            }
            if (user.Password != "" && user.Password.Count() < 4)
            {
                ViewBag.Mess = "Password must have at least 4 symbols!";
                return View("AddSeller");
            }

            ViewBag.Seller = "Seller succesfully added!";
            us.Add(user.Username, user);
            ReadWrite.WriteSeller(user, "~/App_Data/Users.txt");
            return RedirectToAction("Index", "Event");
        }

       
       public ActionResult DeleteUser(string id)
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");

            if(users.ContainsKey(id))
            {
                users[id].Deleted = true;
            }

            ReadWrite.WriteEdit(users, "~/App_Data/Users.txt");

            return RedirectToAction("Index", "Admin");
        }

        public ActionResult DeleteEvent(string id)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");

            if(events.ContainsKey(id))
            {
                events[id].Deleted = true;
            }

            ReadWrite.WriteEditEvent(events, "~/App_Data/Events.txt");

            return RedirectToAction("Index", "Event");
        }

        public ActionResult SuspiciousUsers()
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
            Dictionary<string, Korisnik> sususers = new Dictionary<string, Korisnik>();
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");

            foreach(var us in users)
            {
                if(us.Value.Canceled>=5)
                {
                    sususers[us.Key] = users[us.Key];
                }
            }
            ViewBag.Find = sususers;

            return View();
        }

        public ActionResult BlockUser(string id)
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
            users = ReadWrite.ReadUser("~/App_Data/Users.txt");

            if (users.ContainsKey(id))
            {
                users[id].Blocked = true;
            }

            ReadWrite.WriteEdit(users, "~/App_Data/Users.txt");

            return RedirectToAction("SuspiciousUsers", "Admin");
        }


    }
}