﻿using PR_57_2016.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR_57_2016.Controllers
{
    public class HomeController : Controller
    {
        Dictionary<string, Event> eventsSort = new Dictionary<string, Event>();
        public ActionResult Index()
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            events = ReadWrite.ReadEvent("~/App_Data/Events.txt");

            DictionarySort(events);
            ViewBag.Events = eventsSort;
            return View();
            
        }

        private void DictionarySort(Dictionary<string, Event> dict)
        {
            var dictSort = from objDict in dict orderby objDict.Value.Date descending select objDict;

            eventsSort = dictSort.ToDictionary(t => t.Key, t => t.Value);

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}