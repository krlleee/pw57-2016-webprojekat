﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class Korisnik
    {
        public string Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }

        public string Password { get; set; }

        public string Username { get; set; }
        public DateTime BirthDate { get; set; }
        public string Tickets { get; set; }
        public string Events { get; set; }
        public int Points { get; set; }
        public string Gender { get; set; }
        public UserRole Role { get; set; }
        public UserType Type { get; set; }

        public bool LoggedIn { get; set; }

        public bool Deleted { get; set; }

        public int Canceled { get; set; }
        public bool Blocked { get; set; }

        public Korisnik()
        {
            Username = "";
            Password = "";
            LoggedIn = false;
        }

        public void Logoff()
        {
            Username = "";
            Password = "";
            LoggedIn = false;
        }

        public Korisnik(string username, string password, UserType type)
        {
            Username = username;
            Password = password;
            Type = type;
            LoggedIn = false;
        }

        public Korisnik(string id, string ime, string prezime, string password, string username, DateTime birthDate, string tickets, string events, int points, string gender, UserRole role, UserType type, bool loggedIn, bool deleted,int canceled,bool blocked)
        {
            Id = id;
            Ime = ime;
            Prezime = prezime;
            Password = password;
            Username = username;
            BirthDate = birthDate;
            Tickets = tickets;
            Events = events;
            Points = points;
            Gender = gender;
            Role = role;
            Type = type;
            LoggedIn = loggedIn;
            Deleted = deleted;
            Canceled = canceled;
            Blocked = blocked;
        }

        public bool Login()
        {
            if (Users.users.ContainsKey(Username) && Users.users[Username].Password.Equals(Password))
            {
                LoggedIn = true;
            }
            return LoggedIn;
        }
    }
}