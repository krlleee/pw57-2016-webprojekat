﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class TicketSortHelp
    {
        public TicketSortHelp(string sortType)
        {
            SortType = sortType;
        }

        public TicketSortHelp()
        {
            
        }

        public string SortType { get; set; }
    }
}