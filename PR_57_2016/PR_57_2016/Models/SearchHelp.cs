﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class SearchHelp
    {
        public SearchHelp(string name, string place, DateTime date1, DateTime date2, int price1, int price2)
        {
            Name = name;
            Place = place;
            Date1 = date1;
            Date2 = date2;
            this.price1 = price1;
            this.price2 = price2;
        }

        public SearchHelp() { }

        public string Name { get; set; }
        public string Place { get; set; }

        public DateTime Date1 { get; set; }
        public DateTime Date2 { get; set; }

        public int price1 { get; set; }
        public int price2 { get; set; }
    }
}