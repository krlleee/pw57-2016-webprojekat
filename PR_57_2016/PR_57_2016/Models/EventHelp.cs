﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class EventHelp
    {
        public EventHelp(string id, string name, EventType type, int seatsNum, DateTime date, float price, int ticketRem, EventStatus status, EventPlace address, string sellerId, string poster)
        {
            Id = id;
            Name = name;
            Type = type;
            SeatsNum = seatsNum;
            Date = date;

            Price = price;
            TicketRem = ticketRem;
            Status = status;
            Address = address;
            SellerId = sellerId;
            Poster = poster;
        }

        public EventHelp() { }

        public string Id { get; set; }
        public string Name { get; set; }
        public EventType Type { get; set; }

        public int SeatsNum { get; set; }

        public DateTime Date { get; set; }
        public float Price { get; set; }
        public int TicketRem { get; set; }
        public EventStatus Status { get; set; }
        public EventPlace Address { get; set; }
        public string SellerId { get; set; }
        public string Poster { get; set; }
        public string Place { get; set; }


    }
}