﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class TicketHelp
    {
        public TicketHelp()
        {
        }

        public TicketHelp(string nameEvent, int price, TicketType type, DateTime date, int amount, TicketStatus status,string id,string buyerName,string buyerLast)
        {
            NameEvent = nameEvent;
            Price = price;
            Type = type;
            Date = date;
            Amount = amount;
            Status = status;
            Id = id;
            BuyerName = buyerName;
            BuyerLast = buyerLast;
        }
        public string Id { get; set; }
        public string NameEvent { get; set; }
        public int Price { get; set; }
        public TicketType Type { get; set; }
        public DateTime Date { get; set; }
        public int Amount { get; set; }
        public TicketStatus Status { get; set; }
        public string BuyerName { get; set; }
        public string BuyerLast { get; set; }
    }
}