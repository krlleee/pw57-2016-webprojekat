﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class SortHelp
    {
        public SortHelp(string sortType)
        {
            SortType = sortType;
        }

        public SortHelp() { }

        public string SortType { get; set; }
    }
}