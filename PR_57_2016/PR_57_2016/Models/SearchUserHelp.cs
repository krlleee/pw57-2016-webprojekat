﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class SearchUserHelp
    {
        public SearchUserHelp(string srch)
        {
            Srch = srch;
        }

        public SearchUserHelp() { }
        public string Srch { get; set; }
    }
}