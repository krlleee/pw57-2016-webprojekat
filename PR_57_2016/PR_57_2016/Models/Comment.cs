﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class Comment
    {
        public Comment()
        {
        }

        public Comment(string userId, string eventId, string text,string username,CommentEnum aproved,bool deleted,int grade)
        {
            UserId = userId;
            EventId = eventId;
            Text = text;
            Username = username;
            Aproved = aproved;
            Deleted = deleted;
            Grade = grade;
        }

        public string UserId { get; set; }
        public string EventId { get; set; }
        public string Text { get; set; }
        public string Username { get; set; }

       public CommentEnum Aproved { get; set; }
        public bool Deleted { get; set; }

        public int Grade { get; set; }
    }
}