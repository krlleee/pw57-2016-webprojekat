﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace PR_57_2016.Models
{
    public class ReadWrite
    {
        public static Dictionary<string, Korisnik> ReadUser(string path)
        {
            Dictionary<string, Korisnik> users = new Dictionary<string, Korisnik>();
            path = HostingEnvironment.MapPath(path);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');

                    if (spl[0] != "")
                    {
                        Korisnik user = new Korisnik();
                        string k = spl[0];
                        user.Id = spl[0];
                        user.Ime = spl[1];
                        user.Prezime = spl[2];
                        user.Username = spl[3];
                        user.Password = spl[4];
                        user.Gender = spl[5];
                        user.BirthDate = DateTime.Parse(spl[6]);
                        user.Tickets = spl[7];
                        user.Events = spl[8];
                        user.Points = Int32.Parse(spl[9]);
                        switch (spl[10])
                        {
                            case "administrator":
                                user.Role = UserRole.ADMINISTRATOR;
                                break;
                            case "guest":
                                user.Role = UserRole.GUEST;
                                break;
                            case "seller":
                                user.Role = UserRole.SELLER;
                                break;
                            case "registrated":
                                user.Role = UserRole.REGISTRATED;
                                break;
                            default:
                                break;
                        }

                        switch (spl[11])
                        {
                            case "begginer":
                                user.Type = UserType.BEGGINER;
                                break;
                            case "bronze":
                                user.Type = UserType.BRONZE;
                                break;
                            case "silver":
                                user.Type = UserType.SILVER;
                                break;
                            case "gold":
                                user.Type = UserType.GOLD;
                                break;
                            default:
                                break;

                        }
                        switch(spl[12])
                        {
                            case "False":
                                user.Deleted = false;
                                break;
                            case "True":
                                user.Deleted = true;
                                break;
                            default:
                                break;
                        }
                        user.Canceled = Int32.Parse(spl[13]);
                        switch (spl[14])
                        {
                            case "False":
                                user.Blocked = false;
                                break;
                            case "True":
                                user.Blocked = true;
                                break;
                            default:
                                break;
                        }

                        users.Add(k, user);
                    }
                }
            }
            return users;
        }

        public static void WriteReg(Korisnik user, string path)
        {

            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() + "|" + user.Ime + "|" + user.Prezime + "|" + user.Username + "|" + user.Password + "|" + user.Gender + "|" + user.BirthDate + "|" + "|" + "|" + 0 + "|" + "registrated" + "|" + "begginer" + "|" + "False" + "|" + 0 + "|" + "False");
                writer.Close();
            }
        }

        public static void WriteSeller(Korisnik user, string path)
        {

            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() + "|" + user.Ime + "|" + user.Prezime + "|" + user.Username + "|" + user.Password + "|" + user.Gender + "|" + user.BirthDate + "|" + "|" + "|" + 0 + "|" + "seller" + "|" + "begginer"+"|"+"False" + "|" + 0 + "|" + "False");
                writer.Close();
            }
        }

        public static void WriteEdit(Dictionary<string, Korisnik> users, string path)
        {

            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var user in users.Values)
                {
                    writer.WriteLine();
                    writer.Write(user.Id + "|" + user.Ime + "|" + user.Prezime + "|" + user.Username + "|" + user.Password + "|" + user.Gender + "|" + user.BirthDate + "|" + user.Tickets + "|" + user.Events + "|" + user.Points + "|" + user.Role.ToString().ToLower() + "|" + user.Type.ToString().ToLower()+"|"+user.Deleted+"|"+user.Canceled+"|"+user.Blocked);
                }
                writer.Close();
            }
        }


        public static void EventActive(Dictionary<string, Event> events, string path)
        {

            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var ev in events.Values)
                {
                    writer.WriteLine();
                    writer.Write(ev.Id + "|" + ev.Name + "|" + ev.Type.ToString().ToLower() + "|" + ev.SeatsNum + "|" + ev.Date + "|" + ev.Price + "|" + ev.TicketRem + "|" + ev.Status.ToString().ToLower() + "|" + ev.Address.Id + "|" + ev.SellerId + "|" + ev.Poster+"|"+ev.Deleted);
                }
                writer.Close();
            }
        }

        public static void CommentApproved(List<Comment> comments, string path)
        {

            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var comm in comments)
                {
                    writer.WriteLine();
                    writer.Write(comm.EventId + "|" + comm.UserId + "|" + comm.Text + "|" + comm.Username + "|" + comm.Aproved+"|"+comm.Deleted+"|"+comm.Grade);
                }
                writer.Close();
            }
        }


        public static Dictionary<string,EventPlace> ReadPlace(string path)
        { 
            Dictionary<string, EventPlace> places = new Dictionary<string, EventPlace>();

             path = HostingEnvironment.MapPath(path);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');


                    if (spl[0] != "")
                    {
                        EventPlace ep = new EventPlace();
                        string e = spl[0];
                        ep.Id = spl[0];
                        ep.Place = spl[1];

                        places.Add(e, ep);
                    }
                }
            }
            return places;
        }


        public static void WriteEvent(Event events, string path,string id)
        {
            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() + "|" + events.Name + "|" + events.Type.ToString().ToLower() + "|"  + events.SeatsNum+ "|" + events.Date+ "|" + events.Price+ "|" + events.TicketRem+ "|" + "pasive"+ "|" + events.Address.Id+ "|" + id+ "|" + events.Poster+"|"+"False");
                writer.Close();
            }

        }

        public static void WriteTicket(Ticket ticket, string path)
        {
            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(Guid.NewGuid() + "|" + ticket.Eventt + "|" + ticket.BuyerName + "|" + ticket.Amount + "|" + ticket.Price + "|" + ticket.Status + "|" + ticket.Type + "|" + ticket.Date);
               writer.Close();

            }
        }

        public static void WriteEditTicket(Dictionary<string, Ticket> tickets, string path)
        {
            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var tic in tickets.Values)
                {
                    writer.WriteLine();
                    writer.Write(tic.Id + "|" + tic.Eventt+"|"+tic.BuyerName + "|" + tic.Amount + "|" + tic.Price + "|" + tic.Status + "|" + tic.Type + "|" + tic.Date);
                }
                writer.Close();
            }
        }


        public static void WriteEditEvent(Dictionary<string,Event> eventss,string path)
        {
            string path2 = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path2))
            {
                foreach (var events in eventss.Values)
                {
                    writer.WriteLine();
                    writer.Write(events.Id + "|" + events.Name + "|" + events.Type.ToString().ToLower() + "|" + events.SeatsNum + "|" + events.Date + "|" + events.Price + "|" + events.TicketRem + "|" + events.Status + "|" + events.Address.Id + "|" + events.SellerId + "|" + events.Poster+"|"+events.Deleted);
                }
                writer.Close();
            }
        }


        public static Dictionary<string, Ticket> ReadTicket(string path)
        {
            Dictionary<string, Ticket> tickets = new Dictionary<string, Ticket>();
            path = HostingEnvironment.MapPath(path);


            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');

                    if (spl[0] != "")
                    {
                        Ticket tic = new Ticket();
                        string e = spl[0];
                        tic.Id = spl[0];
                        tic.Eventt = spl[1];
                        tic.BuyerName = spl[2];
                        tic.Amount = Int32.Parse(spl[3]);
                        tic.Price = Int32.Parse(spl[4]);
                        switch(spl[5])
                        {
                            case "RESERVED":
                                tic.Status = TicketStatus.RESERVED;
                                break;
                            case "CANCELED":
                                tic.Status = TicketStatus.CANCELED;
                                break;
                        }
                        switch(spl[6])
                        {
                            case "REGULAR":
                                tic.Type = TicketType.REGULAR;
                                break;
                            case "VIP":
                                tic.Type = TicketType.VIP;
                                break;
                            case "FANPIT":
                                tic.Type = TicketType.FANPIT;
                                break;

                        }
                        tic.Date = DateTime.Parse(spl[7]);

                        tickets.Add(e, tic);
                    }
                }
            }
            return tickets;
        }


        public static Dictionary<string, Event> ReadEvent(string path)
        {
            Dictionary<string, Event> events = new Dictionary<string, Event>();
            Dictionary<string, EventPlace> places = new Dictionary<string, EventPlace>();
            path = HostingEnvironment.MapPath(path);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');

                    if (spl[0] != "")
                    {
                        Event ev = new Event();
                        EventPlace ep = new EventPlace();
                        string e = spl[0];
                        ev.Id = spl[0];
                        ev.Name = spl[1];
                        switch(spl[2])
                        {
                            case "concert":
                                ev.Type = EventType.CONCERT;
                                break;

                            case "cinema":
                                ev.Type = EventType.CINEMA;
                                break;

                            case "festival":
                                ev.Type = EventType.FESTIVAL;
                                break;

                            case "theatre":
                                ev.Type = EventType.THEATRE;
                                break;
                        }
                        ev.SeatsNum = Int32.Parse(spl[3]);
                        ev.Date =DateTime.Parse(spl[4]);
                        ev.Price = Int32.Parse(spl[5]);
                        ev.TicketRem = Int32.Parse(spl[6]);
                        switch(spl[7])
                        {
                            case "active":
                                ev.Status = EventStatus.ACTIVE;
                                break;
                            case "pasive":
                                ev.Status = EventStatus.PASIVE;
                                break;
                        }
                        ep.Id = spl[8];
                        places=ReadPlace("~/App_Data/EventLocations.txt");
                        if(places.ContainsKey(ep.Id))
                        {
                            ep.Place = places[ep.Id].Place;
                        }
                        ev.Address = ep;
                        ev.SellerId = spl[9];
                        ev.Poster = spl[10];
                        switch (spl[11])
                        {
                            case "False":
                                ev.Deleted = false;
                                break;
                            case "True":
                                ev.Deleted = true;
                                break;
                            default:
                                break;
                        }


                        events.Add(e, ev);
                    }
                }
            }
            return events;
        }


        public static void WriteComment(Comment comm,string path)
        {
            path = HostingEnvironment.MapPath(path);

            using (StreamWriter writer = new StreamWriter(path, append: true))
            {
                writer.WriteLine();
                writer.Write(comm.EventId + "|" + comm.UserId + "|" + comm.Text + "|" + comm.Username+"|"+"NOTAPROVED"+"|"+"False"+"|"+comm.Grade);
                writer.Close();
            }
        }


        public static List<Comment> ReadComment(string path)
        {
           List<Comment> comm = new List<Comment>();
            path = HostingEnvironment.MapPath(path);

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.EndOfStream == false)
                {
                    string[] spl = sr.ReadLine().Split('|');

                    if (spl[0] != "")
                    {
                        Comment comment = new Comment();
                        
                        comment.EventId = spl[0];
                        comment.UserId = spl[1];
                        comment.Text = spl[2];
                        comment.Username = spl[3];
                        switch (spl[4]) {

                            case "APROVED":
                                comment.Aproved = CommentEnum.APROVED;
                                break;
                            case "NOTAPROVED":
                                comment.Aproved = CommentEnum.NOTAPROVED;
                                break;
                        }
                        switch (spl[5])
                        {
                            case "False":
                                comment.Deleted = false;
                                break;
                            case "True":
                                comment.Deleted = true;
                                break;
                            default:
                                break;
                        }
                        comment.Grade = Int32.Parse(spl[6]);

                        comm.Add(comment);
                    }
                }
            }
            return comm;
        }


        



    }
}