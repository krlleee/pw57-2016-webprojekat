﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class UserFilter
    {
        public UserFilter()
        {
        }

        public UserFilter(string userFil)
        {
            UserFil = userFil;
        }

        public string UserFil { get; set; }
    }
}