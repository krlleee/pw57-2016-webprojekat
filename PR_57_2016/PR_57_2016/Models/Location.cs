﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class Location
    {
        public Location(int id, string length, string width, string place)
        {
            Id = id;
            Length = length;
            Width = width;
            Place = place;
        }

        public Location() { }

        public int Id { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }

        public string Place { get; set; }
    }
}