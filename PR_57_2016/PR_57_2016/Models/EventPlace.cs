﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class EventPlace
    {
        public EventPlace(string id, string place)
        {
            Id = id;
            Place = place;
        }
        public EventPlace() { }

        public string Id { get; set; }
        public string Place { get; set; }
    }
}