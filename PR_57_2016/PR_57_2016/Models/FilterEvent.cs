﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class FilterEvent
    {
        public FilterEvent()
        {
        }

        public FilterEvent(string filterType)
        {
            FilterType = filterType;
        }

        public string FilterType { get; set; }
    }
}