﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class SearchTicket
    {
        public SearchTicket() { }
        public SearchTicket(string eventName, DateTime dateFrom, DateTime dateTo, int priceFrom, int priceTo,string userId)
        {
            EventName = eventName;
            DateFrom = dateFrom;
            DateTo = dateTo;
            PriceFrom = priceFrom;
            PriceTo = priceTo;
            UserId = userId;
        }

        public string EventName { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }
        public string UserId { get; set; }
    }
}