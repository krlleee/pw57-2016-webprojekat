﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class Ticket
    {
        public Ticket(string id, string eventt,  DateTime date, int price, TicketType type, string buyerName, TicketStatus status, int amount)
        {
            Id = id;
            Eventt = eventt;
            Date = date;
            
            Price = price;
            Type = type;
            BuyerName = buyerName;
            Status = status;
            Amount = amount;
        }

        public Ticket() { }

        public string Id { get; set; }
        public string Eventt { get; set; }
  
        public DateTime Date { get; set; }

        public int Price { get; set; }
        public TicketType Type { get; set; }

        public string BuyerName { get; set; }
        public TicketStatus Status {get;set;}

        public int Amount { get; set; }
       
    }
}