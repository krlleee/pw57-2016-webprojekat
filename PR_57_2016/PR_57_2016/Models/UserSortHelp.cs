﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR_57_2016.Models
{
    public class UserSortHelp
    {
        public UserSortHelp(string shelp)
        {
            Shelp = shelp;
        }
        public UserSortHelp() { }
        public string Shelp { get; set; }
    }
}